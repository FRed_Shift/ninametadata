import os,sys
import json
import pathlib
#import glob
from matplotlib import pyplot as plt
import numpy as np
from scipy.optimize import curve_fit




def Run(inputfilename):
    keep_open=True
    def hyp(x, b):
        return a * np.sqrt(1+(x-loc)**2/b**2) 
    
    folder,filename=os.path.split(inputfilename)
    os.chdir(folder)
  
    #for files in pathlib.Path(folder).glob('*.json'):

    folder,filename=os.path.split(inputfilename)
    fileroot,ext=os.path.splitext(inputfilename)
    os.chdir(folder)
    print('Filename: ', filename)

    with open(os.path.join(folder,filename),'r') as fp:
    #with open(files,'r') as fp:
        data = json.load(fp)

    #print('datalen: ', data)

    
    

    #with open('listfile.txt', 'r') as filehandle:
    #    places = [current_place.rstrip() for current_place in filehandle.readlines()] 
    # print('datalen: ', len(measures))      

    #measures=data["ExposureNumber"]
    #print('datalen: ', len(measures))

    #if keep_open:
    #    print('Press Enter to close')
    #    input()
    ExposureNumber=np.array([m['ExposureNumber'] for m in data])
    ADUMedian=np.array([m['ADUMedian'] for m in data])
    DetectedStars=np.array([m['DetectedStars'] for m in data])
    HFR=np.array([m['HFR'] for m in data])
    FocuserTemp=np.array([m['FocuserTemp'] for m in data])
    FocuserPosition=np.array([m['FocuserPosition'] for m in data])

    ExposureStart=np.array([m['ExposureStart'] for m in data])
    time_stamp= str(ExposureStart[0])[:18]
    x=time_stamp.replace("T", "_").replace(":", "-")

    #print ('test: ', x)




    #if keep_open:
    #    print('Press Enter to close')
    #    input()       
            
    #print('focpos: ', str(FocuserPosition))
    #print('hypval: ', str(hypval))
    print(' ')
    #if keep_open:
    #    print('Press Enter to close')
    #input()

    fig, ax1 = plt.subplots()
    color = 'tab:green'
    ax1.set_title('Focuser Temperature')
    ax1.set_xlabel('Frames')
    ax1.set_ylabel('FocuserTemp', color=color)
    ax1.plot(ExposureNumber, FocuserTemp, color=color)
    ax1.tick_params(axis='y', labelcolor=color)
    plt.savefig(fileroot+'_FocTemp.png')



    #plt.figure(0)
    fig, ax1 = plt.subplots()
    color = 'tab:red'
    ax1.set_title('HFR & ADUMedian')
    ax1.set_xlabel('Frames')
    ax1.set_ylabel('HFR', color=color)
    ax1.plot(ExposureNumber, HFR, color=color)
    ax1.tick_params(axis='y', labelcolor=color)

    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    color = 'tab:blue'
    ax2.set_ylabel('ADUMedian', color=color)  # we already handled the x-label with ax1
    ax2.plot(ExposureNumber, ADUMedian, color=color)
    ax2.tick_params(axis='y', labelcolor=color)

    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(fileroot+'_HFR-ADU.png')



    #plt.figure(1)
    fig, ax1 = plt.subplots()
    color = 'tab:red'
    ax1.set_title('Focuser Position / Stars')
    ax1.set_xlabel('Frames')
    ax1.set_ylabel('FocuserPosition', color=color)
    ax1.plot(ExposureNumber, FocuserPosition, color=color)
    ax1.tick_params(axis='y', labelcolor=color)

    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    color = 'tab:blue'
    ax2.set_ylabel('DetectedStars', color=color)  # we already handled the x-label with ax1
    ax2.plot(ExposureNumber, DetectedStars, color=color)
    ax2.tick_params(axis='y', labelcolor=color)

    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(fileroot+'_FocPos-Stars.png')


    plt.show()


    #plt.show()
        
    #if keep_open:
    #    print('Press Enter to close')
    #    input()


if __name__ == "__main__":
    Run(*sys.argv[1:])
    